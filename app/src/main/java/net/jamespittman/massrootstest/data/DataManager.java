package net.jamespittman.massrootstest.data;

import net.jamespittman.massrootstest.data.remote.RepoService;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

@Singleton
public class DataManager {
    private final RepoService mRepoService;

    @Inject
    public DataManager(RepoService repoService) {
        mRepoService = repoService;
    }

    public Observable<RepoService.SearchResponse> getRepos(String query) {
        return mRepoService.searchRepos(query);
    }
}
