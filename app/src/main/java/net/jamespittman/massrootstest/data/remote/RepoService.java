package net.jamespittman.massrootstest.data.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import net.jamespittman.massrootstest.BuildConfig;
import net.jamespittman.massrootstest.data.model.GitHubRepo;

import java.util.List;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

public interface RepoService {
    String API_ENDPOINT = "https://api.github.com/";

    @GET("search/repositories?per_page=100")
    Observable<SearchResponse> searchRepos(@Query(value = "q", encoded = false) String query);

    class Factory {
        public static RepoService makeRepoService() {
            OkHttpClient okHttpClient = new OkHttpClient();
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                    : HttpLoggingInterceptor.Level.NONE);
            okHttpClient.interceptors().add(loggingInterceptor);
            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .create();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API_ENDPOINT)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            return retrofit.create(RepoService.class);
        }
    }

    class SearchResponse {
        @SerializedName("total_count")
        public int totalCount;
        @SerializedName("incomplete_results")
        public boolean incompleteResults;
        @SerializedName("items")
        public List<GitHubRepo> repos;
    }
}

