package net.jamespittman.massrootstest.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class GitHubRepo implements Parcelable {
    public Integer id;
    public String name;
    //Repository in format user/repoName
    @SerializedName("full_name")
    public String fullName;
    //owner of repository
    public GitHubUser owner;
    @SerializedName("private")
    public Boolean privateRepo;
    public String url;
    //programming language used
    public String language;
    public String description;
    @SerializedName("updated_at")
    public Date updatedAt;

    public GitHubRepo() {
    }

    protected GitHubRepo(Parcel in) {
        id = in.readInt();
        name = in.readString();
        fullName = in.readString();
        owner = in.readParcelable(GitHubUser.class.getClassLoader());
        privateRepo = in.readByte() != 0;
        url = in.readString();
        language = in.readString();
        description = in.readString();
        updatedAt = new Date(in.readLong());
    }

    public static final Creator<GitHubRepo> CREATOR = new Creator<GitHubRepo>() {
        @Override
        public GitHubRepo createFromParcel(Parcel in) {
            return new GitHubRepo(in);
        }

        @Override
        public GitHubRepo[] newArray(int size) {
            return new GitHubRepo[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GitHubRepo repo = (GitHubRepo) o;
        return (id.equals(repo.id));
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 29 * result + (name != null ? name.hashCode() : 0);
        result = 29 * result + (fullName != null ? fullName.hashCode() : 0);
        result = 29 * result + (owner != null ? owner.hashCode() : 0);
        result = 29 * result + (privateRepo != null ? privateRepo.hashCode() : 0);
        result = 29 * result + (url != null ? url.hashCode() : 0);
        result = 29 * result + (language != null ? language.hashCode() : 0);
        result = 29 * result + (description != null ? description.hashCode() : 0);
        result = 29 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(fullName);
        dest.writeParcelable(owner, 0);
        dest.writeByte((byte) (privateRepo ? 1 : 0));
        dest.writeString(url);
        dest.writeString(language);
        dest.writeString(description);
        dest.writeLong(updatedAt.getTime());
    }
}
