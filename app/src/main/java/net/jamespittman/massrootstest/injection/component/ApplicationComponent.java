package net.jamespittman.massrootstest.injection.component;

import android.app.Application;
import android.content.Context;

import net.jamespittman.massrootstest.MassRootsTestApplication;
import net.jamespittman.massrootstest.data.DataManager;
import net.jamespittman.massrootstest.data.remote.RepoService;
import net.jamespittman.massrootstest.injection.ApplicationContext;
import net.jamespittman.massrootstest.injection.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(MassRootsTestApplication massRootsTestApplication);
    @ApplicationContext
    Context context();
    Application application();
    DataManager dataManager();
    RepoService repoService();
}