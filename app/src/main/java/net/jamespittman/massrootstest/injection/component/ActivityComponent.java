package net.jamespittman.massrootstest.injection.component;

import net.jamespittman.massrootstest.injection.PerActivity;
import net.jamespittman.massrootstest.injection.module.ActivityModule;
import net.jamespittman.massrootstest.ui.reposearch.RepoSearchActivity;
import net.jamespittman.massrootstest.ui.repoview.RepoViewerActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(RepoSearchActivity repoSearchActivity);

    void inject(RepoViewerActivity repoViewerActivity);
}


