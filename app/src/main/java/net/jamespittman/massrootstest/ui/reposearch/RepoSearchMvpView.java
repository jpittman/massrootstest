package net.jamespittman.massrootstest.ui.reposearch;

import net.jamespittman.massrootstest.data.model.GitHubRepo;
import net.jamespittman.massrootstest.ui.base.MvpView;

import java.util.List;

public interface RepoSearchMvpView extends MvpView {
    void showProgress(boolean progress);

    void showEmptyMessage(boolean empty);

    void showRepos(List<GitHubRepo> repos);

    void showReposError();
}
