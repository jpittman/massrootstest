package net.jamespittman.massrootstest.ui.reposearch;

import net.jamespittman.massrootstest.data.DataManager;
import net.jamespittman.massrootstest.data.remote.RepoService;
import net.jamespittman.massrootstest.ui.base.BasePresenter;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class RepoSearchPresenter extends BasePresenter<RepoSearchMvpView> {
    private RepoSearchMvpView mMvpView;
    private DataManager mDataManager;
    private Subscription mSubscription;

    @Inject
    public RepoSearchPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(RepoSearchMvpView mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mMvpView = null;
        if (mSubscription != null) mSubscription.unsubscribe();
    }

    public void loadRepos(String query) {
        if (mSubscription != null) mSubscription.unsubscribe();
        mSubscription = getReposObservable(query)
                .subscribe(new Subscriber<RepoService.SearchResponse>() {

                    @Override
                    public void onCompleted() {
                        mMvpView.showProgress(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e("There was an error loading the repo list " + e);
                        mMvpView.showReposError();
                        mMvpView.showProgress(false);
                    }

                    @Override
                    public void onNext(RepoService.SearchResponse response) {
                        mMvpView.showRepos(response.repos);
                    }
                });

    }

    private Observable<RepoService.SearchResponse> getReposObservable(String query) {
        return mDataManager.getRepos(query)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }
}