package net.jamespittman.massrootstest.ui.base;

import android.support.v7.app.AppCompatActivity;

import net.jamespittman.massrootstest.MassRootsTestApplication;
import net.jamespittman.massrootstest.injection.component.ActivityComponent;
import net.jamespittman.massrootstest.injection.component.DaggerActivityComponent;
import net.jamespittman.massrootstest.injection.module.ActivityModule;

public class BaseActivity extends AppCompatActivity {

    private ActivityComponent mActivityComponent;

    public ActivityComponent activityComponent() {
        if (mActivityComponent == null) {
            mActivityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(MassRootsTestApplication.get(this).getComponent())
                    .build();
        }
        return mActivityComponent;
    }
}
