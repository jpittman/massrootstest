package net.jamespittman.massrootstest.ui.reposearch;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.jamespittman.massrootstest.R;
import net.jamespittman.massrootstest.data.model.GitHubRepo;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RepoAdapter extends RecyclerView.Adapter<RepoAdapter.RepoHolder> {
    private List<GitHubRepo> mRepos;
    private Callback mCallback;

    @Inject
    public RepoAdapter() {
        this.mRepos = new ArrayList<>();
    }

    public void setRepos(List<GitHubRepo> repos) {
        mRepos = new ArrayList<>(repos);
    }

    public List<GitHubRepo> getRepos() {
        return mRepos;
    }

    @Override
    public RepoAdapter.RepoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RepoHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_repo, parent, false));
    }

    @Override
    public void onBindViewHolder(RepoAdapter.RepoHolder holder, int position) {
        GitHubRepo repo = mRepos.get(position);
        Picasso.with(holder.itemView.getContext()).load(repo.owner.avatarUrl)
                .fit().centerCrop().into(holder.avatar);
        holder.repoTitle.setText(repo.name);
        holder.description.setText(repo.description);
        holder.language.setText(repo.language);
        holder.userName.setText(repo.owner.name);
        holder.timestamp.setText(repo.updatedAt.toString());
    }

    @Override
    public int getItemCount() {
        return mRepos.size();
    }

    public class RepoHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.repo_title)
        public TextView repoTitle;
        @Bind(R.id.description)
        public TextView description;
        @Bind(R.id.language)
        public TextView language;
        @Bind(R.id.user_name)
        public TextView userName;
        @Bind(R.id.avatar)
        public ImageView avatar;
        @Bind(R.id.timestamp)
        public TextView timestamp;

        public RepoHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.repo_container)
        void onClick() {
            if (mCallback != null) mCallback.onRepoClicked(mRepos.get(getAdapterPosition()));
        }
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    interface Callback {
        void onRepoClicked(GitHubRepo repo);
    }
}
