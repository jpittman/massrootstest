package net.jamespittman.massrootstest.ui.repoview;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import net.jamespittman.massrootstest.R;
import net.jamespittman.massrootstest.data.model.GitHubRepo;
import net.jamespittman.massrootstest.ui.base.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RepoViewerActivity extends BaseActivity {
    public static final String selectionKey = "key_selection";
    public static final String endpoint = "https://github.com/";
    @Bind(R.id.repo_web_view)
    WebView mWebView;

    public static Intent getStartIntent(Context context, GitHubRepo repo) {
        Intent intent = new Intent(context, RepoViewerActivity.class);
        intent.putExtra(selectionKey, repo);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_repo_view);
        ButterKnife.bind(this);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.getSettings().setBuiltInZoomControls(false);
        mWebView.getSettings().setSupportZoom(false);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        GitHubRepo thisRepo = getIntent().getParcelableExtra(selectionKey);
        mWebView.loadUrl(endpoint + thisRepo.fullName);
    }
}
