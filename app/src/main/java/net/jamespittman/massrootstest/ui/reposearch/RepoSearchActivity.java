package net.jamespittman.massrootstest.ui.reposearch;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import net.jamespittman.massrootstest.R;
import net.jamespittman.massrootstest.data.model.GitHubRepo;
import net.jamespittman.massrootstest.ui.base.BaseActivity;
import net.jamespittman.massrootstest.ui.repoview.RepoViewerActivity;

import java.util.ArrayList;
import java.util.List;


import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RepoSearchActivity extends BaseActivity implements RepoSearchMvpView,
        SearchView.OnQueryTextListener, RepoAdapter.Callback {
    public static final String reposKey = "key_repos";
    @Inject
    RepoSearchPresenter mRepoSearchPresenter;
    @Inject
    RepoAdapter mRepoAdapter;

    @Bind(R.id.recycler_view_repos)
    RecyclerView mRepoRecycler;
    @Bind(R.id.text_no_repos)
    TextView mNoReposText;
    @Bind(R.id.progress)
    ProgressBar mProgress;
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_repo_search);
        ButterKnife.bind(this);
        mRepoSearchPresenter.attachView(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRepoRecycler.setLayoutManager(linearLayoutManager);
        mRepoAdapter = new RepoAdapter();
        mRepoAdapter.setCallback(this);
        mRepoRecycler.setAdapter(mRepoAdapter);
        setSupportActionBar(toolbar);
        if (savedInstanceState != null) {
            List<GitHubRepo> repos = savedInstanceState.getParcelableArrayList(reposKey);
            showRepos(repos);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(reposKey,
                new ArrayList<Parcelable>(mRepoAdapter.getRepos()));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRepoSearchPresenter.detachView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_repo_search, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        showEmptyMessage(false);
        showProgress(true);
        mRepoSearchPresenter.loadRepos(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onRepoClicked(GitHubRepo repo) {
        startActivity(RepoViewerActivity.getStartIntent(this, repo));
    }

    @Override
    public void showProgress(boolean show) {
        if (show && mRepoAdapter.getItemCount() == 0) {
            mProgress.setVisibility(View.VISIBLE);
        } else {
            mProgress.setVisibility(View.GONE);
        }

    }

    @Override
    public void showEmptyMessage(boolean show) {
        if (show) {
            mNoReposText.setVisibility(View.VISIBLE);
        } else {
            mNoReposText.setVisibility(View.GONE);
        }
    }

    @Override
    public void showRepos(List<GitHubRepo> newRepos) {
        mRepoAdapter.setRepos(newRepos);
        mRepoAdapter.notifyDataSetChanged();
        showEmptyMessage(false);
    }

    @Override
    public void showReposError() {
        Toast.makeText(this, "Error loading repositories.", Toast.LENGTH_LONG).show();
    }
}