package net.jamespittman.massrootstest;

import android.app.Application;
import android.content.Context;

import net.jamespittman.massrootstest.injection.component.ApplicationComponent;
import net.jamespittman.massrootstest.injection.component.DaggerApplicationComponent;
import net.jamespittman.massrootstest.injection.module.ApplicationModule;

import timber.log.Timber;

public class MassRootsTestApplication extends Application {

    private static Context context;
    ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) Timber.plant(new Timber.DebugTree());
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        applicationComponent.inject(this);
        MassRootsTestApplication.context = getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        return applicationComponent;
    }

    public static MassRootsTestApplication get(Context context) {
        return (MassRootsTestApplication) context.getApplicationContext();
    }

    public static Context getAppContext() {
        return MassRootsTestApplication.context;
    }
}
